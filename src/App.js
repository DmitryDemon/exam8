import React, { Component, Fragment } from 'react';
import {Switch, Route, NavLink as RouterNavLink} from "react-router-dom";
import {Collapse, Container, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";

import SubmitNewQuote from "./containers/SubmitNewQuote";
import QuotesList from "./containers/QuotesList";
import EditQuote from "./containers/EditQuotes";

import './App.css';

class App extends Component {
  render() {
    return (
      <Fragment>
          <Navbar dark color='dark' expand="md">
              <NavbarBrand>Quotes Central</NavbarBrand>
              <NavbarToggler/>
              <Collapse isOpen navbar>
                  <Nav className="ml-auto" navbar>
                      <NavItem>
                          <NavLink tag={RouterNavLink} to='/' exact>Quotes</NavLink>
                      </NavItem>
                      <NavItem>
                          <NavLink tag={RouterNavLink} to='/add-quote'>Submit new quote</NavLink>
                      </NavItem>
                  </Nav>
              </Collapse>
          </Navbar>
          <Container>
              <Switch>
                  <Route path='/' exact component={QuotesList}/>
                  <Route path='/add-quote' exact component={SubmitNewQuote}/>
                  <Route path='/quotes/:id/edit' component={EditQuote}/>
                  <Route path='/quotes/:categoryId' component={QuotesList}/>
                  <Route render={()=> <h1>not found</h1> }/>
              </Switch>
          </Container>
      </Fragment>
    );
  }
}

export default App;
