import React, {Component} from 'react'

import {CATEGORIES} from "../constants";
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";

class QuoteForm extends Component {

    constructor(props){
        super(props);
        if (props.quote){
            this.state = {...props.quote}
        } else {
            this.state = {
                category:Object.keys(CATEGORIES)[0],
                author:'',
                quoteText:'',
            };
        }
    }


    valueChanged = event =>{
        const {name, value} = event.target;

        this.setState({[name]:value})
    };

    submitHendler = event => {
        event.preventDefault();
        this.props.onSubmit({...this.state})
    };

    render() {
        return (
            <Form className="QuoteForm" onSubmit={this.submitHendler}>
                <FormGroup row>
                    <Label for="category" sm={2}>Category</Label>
                    <Col sm={10}>
                        <Input type="select" name="category" id="category"
                               value={this.state.category} onChange={this.valueChanged}
                        >
                            {Object.keys(CATEGORIES).map(categotyID => (
                                <option key={categotyID} value={categotyID}>{CATEGORIES[categotyID]}</option>
                            ))}
                        </Input>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="author" sm={2}>Author</Label>
                    <Col sm={10}>
                        <Input type="text" name="author" id="author" placeholder='Quote Author'
                               value={this.state.author} onChange={this.valueChanged}
                        />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="quoteText" sm={2}>Quote Text</Label>
                    <Col sm={10}>
                        <Input type="textarea" name="quoteText" id="quoteText" placeholder='Quote Text'
                               value={this.state.quoteText} onChange={this.valueChanged}
                        />
                    </Col>
                </FormGroup>

                <FormGroup>
                    <Col sm={{size:10,offset:2}}>
                        <Button type='submit'>Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default QuoteForm;