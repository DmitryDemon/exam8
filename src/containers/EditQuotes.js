import React, {Component, Fragment} from 'react';
import axios from '../axios-quotes'

import QuoteForm from "../components/QuoteForm";

class EditQuote extends Component {

    state = {
      quote: null,
    };

    getQuoteUrl = () => {
        const id = this.props.match.params.id;
        return 'quotes/' + id + '.json';
    };

    componentDidMount() {

        axios.get(this.getQuoteUrl()).then(response => {
            this.setState({quote: response.data});
        })
    }

    editQuote = quote => {
        axios.put(this.getQuoteUrl(), quote).then(() => {
            this.props.history.goBack();
        });
    };




    render() {

        console.log(this.state.quote);

        let form = <QuoteForm
            onSubmit={this.editQuote}
            quote={this.state.quote}
        />;

        if (!this.state.quote) {
            form = <div>Loading...</div>
        }

        return (
            <Fragment>
                <h1>Edit a quote</h1>
                {form}
            </Fragment>
        );
    }
}

export default EditQuote;
