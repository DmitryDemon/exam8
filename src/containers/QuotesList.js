import React, {Component} from 'react';
import {
    Button,
    Card,
    CardBody,
    CardColumns,
    CardFooter,
    CardText,
    CardTitle,
    Col,
    Nav,
    NavItem,
    NavLink,
    Row
} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import {CATEGORIES} from "../constants";
import axios from '../axios-quotes';

class QuotesList extends Component {

    state = {
        quotes: null
    };

    loadData() {
        let url = 'quotes.json';
        const categoryId = this.props.match.params.categoryId;

        if (categoryId){
            url += `?orderBy="category"&equalTo="${categoryId}"`;
        }
        axios.get(url).then(response => {
            if (!response.data) return;
            const quotes = Object.keys(response.data).map(id => {
                return {...response.data[id], id}
            });
            this.setState({quotes});
        })
    }

    componentDidMount() {
        this.loadData();
    }

    componentDidUpdate(prevProps) {
        if (this.props.match.params.categoryId !== prevProps.match.params.categoryId) {
            this.loadData();
        }
    }

    handleRemove = (id) => {
        axios.delete('quotes/' + id + '.json')
            .then(() => {
                this.loadData();
            })
            .catch((error) => {
                console.log(error);
            });
    };

    

    render() {

        let quotes = null;

        if (this.state.quotes) {
            quotes = this.state.quotes.map(quote =>(
                <Card key={quote.id}>
                    <CardBody>
                        <CardTitle>{quote.author}</CardTitle>
                        <CardText>{quote.quoteText}</CardText>
                    </CardBody>
                    <CardFooter>
                        <RouterNavLink to={'/quotes/'+quote.id+'/edit'}>
                            <Button>Edit</Button>
                        </RouterNavLink>
                        <Button onClick={()=>this.handleRemove(quote.id)}  style={{marginLeft:'90px'}}>Delete</Button>
                    </CardFooter>
                </Card>
            ))
        }

        return (
            <Row style={{marginTop:'20px'}}>
                <Col sm={3}>
                    <Nav vertical>
                        <NavItem>
                            <NavLink tag={RouterNavLink} to='/' exact>All</NavLink>
                        </NavItem>
                        {Object.keys(CATEGORIES).map(categoryId => (
                            <NavItem key={categoryId}>
                                <NavLink
                                    tag={RouterNavLink}
                                    to={'/quotes/'+ categoryId}
                                    exact
                                >
                                    {CATEGORIES[categoryId]}
                                </NavLink>
                            </NavItem>
                        ))}

                    </Nav>
                </Col>
                <Col sm={9}>
                    <CardColumns>
                        {quotes}
                    </CardColumns>
                </Col>
            </Row>
        );
    }
}

export default QuotesList;
