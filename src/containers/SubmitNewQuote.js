import React, {Component, Fragment} from 'react';
import axios from '../axios-quotes'

import QuoteForm from "../components/QuoteForm";

class SubmitNewQuote extends Component {

    submitNewQuote = quote => {
        axios.post('quotes.json', quote).then(response =>{
            this.props.history.replace('/');
        })
    };

    render() {
        return (
           <Fragment>
                <h1>Submit new quote</h1>
                <QuoteForm onSubmit={this.submitNewQuote}/>
           </Fragment>
        );
    }
}

export default SubmitNewQuote;
